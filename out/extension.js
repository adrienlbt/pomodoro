"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const path = require("path");
const arrayGifUrl = {
    'clock': 'https://media.giphy.com/media/QumnfihD4ibhRUqGcH/giphy.gif',
    'rumble': 'https://media.giphy.com/media/3aGZA6WLI9Jde/giphy.gif',
    'bolt': 'https://media.giphy.com/media/26zzdF4XDAvGsWCVG/giphy.gif'
};
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "pomodoro" is now active!');
    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    // Only allow a single Pomodoro view
    let panel = undefined;
    let disposable = vscode.commands.registerCommand('pomodoro.start', () => {
        // The code you place here will be executed every time your command is executed
        panel = vscode.window.createWebviewPanel('pomodoro', 'Pomodoro Start', vscode.ViewColumn.One, {
            localResourceRoots: [vscode.Uri.file(path.join(context.extensionPath, 'assets'))],
            // Enable scripts in the webview
            enableScripts: true,
            // Maintien le contenu lorsque la webview n'est plus au premier plan
            retainContextWhenHidden: true
        });
        // Chargement du css externe
        const styleOnDiskPath = vscode.Uri.file(path.join(context.extensionPath, 'assets', 'style.css'));
        const cssFileSrc = panel.webview.asWebviewUri(styleOnDiskPath);
        // Chargement du js externe
        const scriptOnDiskPath = vscode.Uri.file(path.join(context.extensionPath, 'assets', 'main.js'));
        const scriptFileSrc = panel.webview.asWebviewUri(scriptOnDiskPath);
        let iteration = 0;
        const gif = iteration++ % 2 ? 'clock' : 'rumble';
        panel.title = 'Pomodoro GIF';
        panel.webview.html = getWebviewContent(gif, cssFileSrc, scriptFileSrc);
        // Display a message box to the user
        vscode.window.showInformationMessage('Pomodoro start');
        panel.webview.onDidReceiveMessage(message => {
            switch (message.command) {
                case 'stop':
                    vscode.window.showInformationMessage(message.text);
                    return;
            }
        }, undefined, context.subscriptions);
    });
    let stopCommand = vscode.commands.registerCommand('pomodoro.stop', () => {
        if (!panel) {
            return;
        }
        // Send a message to our webview.
        // You can send any JSON serializable data.
        panel.webview.postMessage({ command: 'stop' });
        // Display a message box to the user
        vscode.window.showInformationMessage('Pomodoro stop');
    });
    let restartCommand = vscode.commands.registerCommand('pomodoro.restart', () => {
        if (!panel) {
            return;
        }
        // Send a message to our webview.
        // You can send any JSON serializable data.
        panel.webview.postMessage({ command: 'restart' });
        // Display a message box to the user
        vscode.window.showInformationMessage('Reprise du pomodoro en cours');
    });
    context.subscriptions.push(disposable);
    context.subscriptions.push(stopCommand);
    context.subscriptions.push(restartCommand);
}
exports.activate = activate;
function getWebviewContent(gifUrl, cssFileSrc, scriptFileSrc) {
    return `<!DOCTYPE html>
	<html lang="en">

		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link rel="stylesheet" type="text/css" href="${cssFileSrc}">
			<title>Cat Coding</title>
		</head>

		<body>

			<h1>Pomodoro</h1>

			<img src="${arrayGifUrl[gifUrl]}" width="300" />

			<div id="timer"></div>

			<div class="button pause" onclick="pauseCommandFunction()"></div>

			<div class="button play" onclick="playCommandFunction()"></div>

			<script src="${scriptFileSrc}"></script>

			<script>

				const vscode = acquireVsCodeApi();

				// Handle the message inside the webview
				window.addEventListener('message', event => {

					const message = event.data; // The JSON data our extension sent

					switch (message.command) {

						case 'stop':
							break;

						case 'restart':
							break;


					}

				});


    		</script>

		</body>

	</html>`;
}
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map