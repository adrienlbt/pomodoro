let dateObjectDisplayInTimer = null
let flagCoutnerIsPaused = false

initialize(1);

var countDownInterval = setInterval(function () {

    var dateObjectReference = new Date("2020-01-01 0:0:0")
    
    if(!flagCoutnerIsPaused) {
    
        dateObjectDisplayInTimer.setSeconds(dateObjectDisplayInTimer.getSeconds() - 1)
        showToTimer()

    }

    if (dateObjectDisplayInTimer - dateObjectReference <= 0) {
        clearInterval(countDownInterval)
    }


}, 1000)

function initialize(integerValueStartInMinutes) {

    dateObjectDisplayInTimer = convertsIntegerMinutesToDateObject(integerValueStartInMinutes)

    showToTimer()

}


function convertsIntegerMinutesToDateObject(intergerMinutes) {

    var hours = Math.floor(intergerMinutes / 60);
    var minutes = intergerMinutes % 60;
    var seconds = Math.floor((intergerMinutes % (1000 * 60)) / 1000);

    return new Date("2020-01-01 " + hours + ":" + minutes + ":" + seconds);

}


function showToTimer() {

    let element = document.getElementById("timer")

    element.innerHTML = dateObjectDisplayInTimer.getHours() + ":" + dateObjectDisplayInTimer.getMinutes() + ":" + dateObjectDisplayInTimer.getSeconds()

}


function stopCommandFunction() {

    clearInterval(countDownInterval);

    vscode.postMessage({
        command: 'stop',
        text: 'Pomodro Stop'
    })
}

function pauseCommandFunction()
{
    flagCoutnerIsPaused = true;

    vscode.postMessage({
        command: 'stop',
        text: 'Pomodoro en pause. Temps restant : ' + dateObjectDisplayInTimer.getHours() + ":" + dateObjectDisplayInTimer.getMinutes() + ":" + dateObjectDisplayInTimer.getSeconds()
    })
}

function playCommandFunction() {

    flagCoutnerIsPaused = false;

    vscode.postMessage({
        command: 'restart',
        text: 'Pomodoro en pause. Temps restant : ' + dateObjectDisplayInTimer.getHours() + ":" + dateObjectDisplayInTimer.getMinutes() + ":" + dateObjectDisplayInTimer.getSeconds()
    })

}